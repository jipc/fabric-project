# 法布瑞Fabric的农产品溯源系统配置文档

## 常用指令

查询docker容器正在运行的服务    `docker ps `

**Docker ps 命令**

-a : 显示所有的容器，包括未运行的。

查看当前文件`ls`

复制 `cp fabric_install/bootstrap.sh .`到根目录

后退目录 `cd ..`

打开目录 `cd`

回根目录 `cd ~`

停止程序 `docker stop ID`

stop停止所有容器 `docker stop $(docker ps -a -q)`

删除所有已经停止的容器 ` docker rm $(docker ps -a -q)`

创建文件夹`mkdir` 

使用指令 **cp** 将当前目录 **test/** 下的所有文件复制到新目录 **newtest** 下，输入如下命令

`cp –r test/ newtest   `

进入编译

`vim 文件名` ,快速查找`/`

运行后面的文件`./ +文件名`

删除文件夹`rm -rf 文件`

``kill -9 pid``等于kill -s 9 pid，表示强制，尽快终止一个进程。

快速查找后台运行的服务 `ps -ef | grep 名称`,“ xx --color=auto redis”这是您使用grep命令查询redis关键词时默认的输出。

## 服务器安装ubuntu

==记得重置密码==，不然连不上，xshell连接

## 安装docker与docker-compose

##### 安装docker
 curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
##### 将用户添加到docker组

 sudo usermod -aG docker $USER
##### 更新用户组
 newgrp docker 
##### 安装docker-compose

**Docker-Compose** 是用来管理容器的，类似用户容器管家，我们有N多台容器或者应用需要启动的时候，如果手动去操作，是非常耗费时间的

```
curl -L "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```



##### 增加运行权限
 `sudo chmod +x /usr/local/bin/docker-compose`
##### 测试是否安装成功，有输出内容即可
 docker-compose 

##### 给docker换源 编辑 /etc/docker/daemon.json 写入以下内容保存
{
  "registry-mirrors": ["https://punulfd2.mirror.aliyuncs.com"]
}
##### 重启docker
`sudo systemctl restart docker`

##### 克隆项目

`git clone https://gitee.com/bigfishw/blockchain-aptrace-fabric.git`

## 安装go语言环境

##### 下载二进制包

` wget https://golang.googl	e.cn/dl/go1.18.linux-amd64.tar.gz`

##### 将下载的二进制包解压至 /usr/local目录 

`sudo tar -C /usr/local -xzf go1.18.linux-amd64.tar.gz`

##### 将 /usr/local/go/bin 目录添加至 PATH 环境变量 ,这里需要学会vim基本用法

` vim ~/.bashrc `
 将 export PATH=$PATH:/usr/local/go/bin 输入后保存退出

##### 更新环境变量

` source  ~/.bashrc `

##### 创建文件夹

回到根目录创 `mkdir go`

##### 测试go是否安装成功
 回到根目录`go`

 ` go env -w GO111MODULE=on`
 #以下代理三选一
  go env -w GOPROXY=https://goproxy.cn,direct
  go env -w GOPROXY=https://goproxy.io,direct
  go env -w GOPROXY=https://mirrors.aliyun.com/goproxy/
————————————————

##### 开发者开防火墙

![image-20220910124105683](C:\Users\72177\AppData\Roaming\Typora\typora-user-images\image-20220910124105683.png)

## 安装nvm以及换源

 `wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash`

 ##### 向 ~/.bashrc 写入（使用vim）以下内容后source ~/.bashrc
`export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm`

## 安装与启动fastDFS，分布式文件系统

##### 拉取docker镜像

` docker pull delron/fastdfs`

##### 启动tracker容器
 `docker run -dti --network=host --name tracker -v /var/fdfs/tracker:/var/fdfs -v /etc/localtime:/etc/localtime delron/fastdfs tracker`

##### 启动storage容器
` docker run -dti  --network=host --name storage -e TRACKER_SERVER=39.98.172.171:22122 -v /var/fdfs/storage:/var/fdfs  -v /etc/localtime:/etc/localtime  delron/fastdfs storage`

##### 关掉docker里的fastDFS，分布式文件系统

先停止的容器 ` docker stop $(docker ps -a -q)`

再删掉 ` docker rm $(docker ps -a -q)`



## 安装与启动redis

##### 下载redis源代码
` wget https://download.redis.io/releases/redis-6.2.6.tar.gz`

##### 解压redis

` tar xzf redis-6.2.6.tar.gz`

##### 安装redis
` cd redis-6.2.6`
` make`

 ##### 修改redis.conf

vi 进入redis.conf改

`vi redis.conf`

==回滚的redis记得改密码==

 将redis.conf 里的redis.conf 中 bind 127.0.0.1 这一行注释掉，则任意IP都可以访问；
 找到 protected-mode yes 改为 protected-mode no；保存之后重启redis

退出`esc :wq`

##### 启动redis

进入==src目录==然后 `cd redis-6.2.6`

前台运行`./redis-server ../redis.conf`

后台运行`./redis-server ../redis.conf &`

启动不了可能会被占用

**②使用kill杀死该进程**

```javascript
kill -9 3086 
```



## 安装与启动mysql 8
##### 安装mysql
拉取 `docker pull mysql`
运行` docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234 --name mysql -d mysql`

数据库安装 `sudo apt-get install mysql-client`

1、查询docker容器服务   ` docker ps`
2、进入MySQL服务     `  docker exec -it mysql bash`
3、登录MySQL       `    mysql -uroot -p`

##### 常用指令

查看现有的数据库`show databases;`

##### 登陆mysql且使远程可访问 这里地址是docker网络的地址 使用命令查看：ip a 
` mysql -h 172.17.0.1 -P 3306 -u root -p`
` select host,user from mysql.user;`
` update mysql.user set host = '%' where user = 'root';`
需用flush privileges刷新MySQL的系统权限相关表

` flush privileges;` 

` exit`

## 克隆仓库,拉取fabric镜像
##### 克隆仓库
`git clone https://gitee.com/steakliu/Blockchain-APTrace-Fabric`

##### 拉取镜像 下边几行一次性复制
`docker pull hyperledger/fabric-peer:1.2.0 && 
docker pull hyperledger/fabric-orderer:1.2.0 && 
docker pull hyperledger/fabric-ca:1.2.0 && 
docker pull hyperledger/fabric-tools:1.2.0 && 
docker pull hyperledger/fabric-ccenv:1.2.0 && 
docker pull hyperledger/fabric-baseimage:0.4.10 && 
docker pull hyperledger/fabric-baseos:0.4.10 && 
docker pull hyperledger/fabric-couchdb:0.4.10`

##### 给镜像打标签 下边几行一次性复制
`docker tag hyperledger/fabric-peer:1.2.0 hyperledger/fabric-peer && 
docker tag hyperledger/fabric-orderer:1.2.0 hyperledger/fabric-orderer && 
docker tag hyperledger/fabric-ca:1.2.0 hyperledger/fabric-ca && 
docker tag hyperledger/fabric-tools:1.2.0 hyperledger/fabric-tools && 
docker tag hyperledger/fabric-ccenv:1.2.0 hyperledger/fabric-ccenv && 
docker tag hyperledger/fabric-baseimage:0.4.10  hyperledger/fabric-baseimage && 
docker tag hyperledger/fabric-baseos:0.4.10 hyperledger/fabric-baseos && 
docker tag hyperledger/fabric-couchdb:0.4.10 hyperledger/fabric-couchdb `

## 区块链网络
##### 进入basic_network目录
` cd /home/ubuntu/Blockchain-APTrace-Fabric/blockchain-trace-bcnetwork/basic-network`

##### 给脚本增加可执行权限

这个命令是为sh文件增加可执行权限

 `chmod +x start.sh`
##### 运行脚本，启动区块链网络，仔细检查有无报错 如error等
` ./start.sh`

##### 若提示超时则修改 docker-compose.yml 文件 在order和peer的 environment 里添加：

`GODEBUG=netdns=go`

##### 重启网络(关掉区块链网络，链码会自动关闭)
` docker-compose down -v`
` ./start.sh`



## 安装链码和依赖
##### 切换到webapp目录
` cd /home/ubuntu/Blockchain-APTrace-Fabric/blockchain-trace-bcnetwork/webapp`

##### 给脚本增加可执行权限
` chmod +x *.sh `

##### 安装链码 
` ./start.sh `

##### 安装node8

 `nvm install 8`

可以设置shell中的默认node 版本的` nvm alias default 8`

`nvm use 12`

##### 换源
 `npm config set registry https://registry.npm.taobao.org`
##### 安装依赖
` npm install`
如果安装报错，试一下`sudo apt install g++`

node版本问题安装会出一些问题，尽量保持版本为12.*

查看node版本 `node -v`

##### 用之前装的nvm来管理Node

`nvm install 12`
`nvm use 12`

**运行此程序需要Fabric client和Fabric CA client软件包，您必须通过"npm install“在文件夹中进行安装，如果此操作不成功，请删除node_modules文件夹，然后重新安装，您可以使用"rm -rf node_modules”将其删除。**

==启动中间件之前需要启动网络安装链码==

## 中间件node app.js

```bash
 #安装用户密钥
 node enrollAdmin.js
 node registerUser.js
 #若安装用户密钥失败则删除 hfc-key-store/
 rm -rf hfc-key-store/
 node rebuild
 #启动中间件
 node app.js
```

前台运行`node app.js`

如果需要让其常驻后台，需要安装pm2

`npm install -g pm2`

> 启动：pm2 start app.js

> 停止：pm2 stop app.js

找到Nodeapp.js `ps -ef | grep node` 强制杀死`kill -9 3086 `

## 本地安装node 12
 ##### 下载nvm安装包并安装
 https://github.com/coreybutler/nvm-windows/releases/download/1.1.9/nvm-setup.zip

报错的话下载1.1.7

##### nvm 镜像的配置

1.在 nvm 的安装路径下，找到 settings.txt，在后面加上这两行

`node_mirror: https://npm.taobao.org/mirrors/node/`
`npm_mirror: https://npm.taobao.org/mirrors/npm/`

 ##### 使用管理员权限打开cmd，安装node v12 
` nvm install 12 `

`nvm use 12`

查看安装了哪些`nvm ls`

 ##### 将node12设为默认
 nvm alias default 12.x.x
 ##### 换源
 先执行这个`npm config set registry https://registry.npm.taobao.org`

##### 安装教程

1.先部署好区块链网络（blockchain-trace-bcnetwork,这里需要先拉取好所需的docker镜像，fabric-orderer,fabric-peer,fabric-counchdb,fabric-tools, fabric-ca，然后tag为latest）： 将traceNetwork上传至服务器（也可自己搭建），**进入basic-network目录中，启动start.sh脚本（./start.sh）**, 启动成功后**进入webapp目录，启动start.sh脚本**（此脚本是安装==智能合约==，它里面包含了其他几个脚本，可以自己观看）， 启动成功后看一下docker容器，不出意外的话会安装了==6个chaincode==,安装成功后执行`node enrollAdmin.js `和 `node registerUser.js`(这里如果没有生成成功，执行`npm install fabric-client`后再试) 生成对应的密钥文件后， 最后启动node服务，命令为 node app.js ， 如果需要让其常驻后台，需要安装pm2，然后执行启动 pm2 start app.js , 停止 pm2 stop app.js ， （环境变量需要有node，npm , golang）

## 本地使用idea导入项目并修改

**1.Blockchain-APTrace-Fabric/blockchain-trace-pc/src 中 main.js 将localhost改为服务器地址**

**2.Blockchain-APTrace-Fabric/blockchain-trace-basic-data/fabric-admin/src/main/resources 中的application.yml ：**
**host: 127.0.0.1 （改为服务器地址）**
**tracker-list: 127.0.0.1:22122（改为服务器地址）**
**address: http://127.0.0.1:8888/（改为服务器地址）**

**3.application-druid.yml(同一级):**
**url: jdbc:mysql://127.0.0.1:3306/blockchain（修改服务器地址与实际数据库名）**
**password: root (修改为实际密码)**

**4.application.yml(同一级):**
**profile: D:/fabric/uploadPath （改为实际路径）**

**5.logback.xml(同一级):**
**value=“/home/fabric/logs” （value 改为实际路径）****

## 目录简介

**blockchain-trace-applets 是小程序的**
**blockchain-trace-basic-data 管理系统后台**
**blockchain-trace-bcnetwork 区块链网络**
**blockchain-trace-pc 前端**
**install-fabric-env fabric框架安装相关**
**Blockchain-APTrace-Fabric / blockchain-trace-bcnetwork / chaincode 链码**
**Blockchain-APTrace-Fabric / blockchain-trace-bcnetwork / basic-network 区块链网络**
**克隆/下载 新建文件 Blockchain-APTrace-Fabric / blockchain-trace-bcnetwork / webapp 中间件**

## 启动项目流程

先停止的容器 ` docker stop $(docker ps -a -q)`

再删掉 ` docker rm $(docker ps -a -q)`

##### 启动mysql

每次重装都会掉数据

##### 用Navitcat导入数据库到服务器里

`E:\代码\blockchain-aptrace-fabric\blockchain-trace-basic-data\sql`

##### 启动**fastDFS**分布式系统

##### 启动区块链网络 

##### 安装链码

装不上重启区块链网络然后再装



##### 启动中间件 webapp目录 node app.js  

##### 启动redis

##### 在blockchain-trace-pc中 启动前端

` npm run dev`

##### 启动法布瑞项目（idea）启动后端

跑不起来换jkd1.8



## 关闭流程

##### 关掉区块链网络，链码会自动关闭

##### 关掉redis和Nodeapp.js

##### 关掉docker里的fastDFS，分布式文件系统

# 界面展示

## 登录界面

![image-20221108231544622](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108231547469.png)

### 消费者溯源

![image-20221108231606205](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108231606205.png)

### 查看区块详情![image-20221108231632281](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108231632281.png)

### 厂商质监![image-20221108231713803](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108231713803.png)

![image-20221108231733557](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108231733557.png)

## 首页

![image-20221108231818177](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108231818177.png)

### 系统管理

### 用户管理![image-20221108231901276](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108231901276.png)

![image-20221108232033073](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232033073.png)

### 角色管理![image-20221108232049809](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232049809.png)

### 菜单管理

![image-20221108232113782](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232113782.png)

### 部门管理![image-20221108232202813](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232202813.png)

### 岗位管理![image-20221108232214191](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232214191.png)

### 字典管理![image-20221108232228381](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232228381.png)

![image-20221108232331830](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232331830.png)

### 参数设置![image-20221108232346851](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232346851.png)

### 通知公告![image-20221108232405401](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232405401.png)

### 日志管理

#### 操作日志![image-20221108232429941](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232429941.png)

#### 登录日志![image-20221108232443187](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232443187.png)

## 系统监控

### 在线用户

![image-20221108232530251](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232530251.png)

### 定时任务![image-20221108232541683](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108232541683.png)

### 数据监控

#### 首页

![image-20221108233017985](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233017985.png)

#### 数据源

![image-20221108233242188](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233242188.png)

#### SQL监控

![image-20221108233256182](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233256182.png)

#### SQL防火墙

![image-20221108233306759](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233306759.png)

#### Web应用

![image-20221108233323364](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233323364.png)

#### URI监控

![image-20221108233335792](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233335792.png)

#### Session监控

![image-20221108233348736](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233348736.png)

#### Spring监控

![image-20221108233358856](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233358856.png)

#### JSON API

![image-20221108233410433](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233410433.png)

### 服务监控

![image-20221108233530472](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233530472.png)

![image-20221108233541444](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221108233541444.png)

### 系统工具

#### 表单构建

可以一键生成vue代码，自己设计前端页面

![image-20221111145717741](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111145717741.png)

#### 代码生成

![image-20221111145837539](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111145837539.png)

#### 系统接口

![image-20221111150104058](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111150104058.png)

![image-20221111150138496](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111150138496.png)

## 农作物管理（上链）

![image-20221111151329442](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111151329442.png)

### 添加作物

![image-20221111150658698](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111150658698.png)

![image-20221111150828915](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111150828915.png)

### 周期记录

![image-20221111151102386](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111151102386.png)



### 联系运输

![image-20221111150906618](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111150906618.png)

### 物流追踪

![image-20221111151236797](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111151236797.png)

车在动！！

### BUG

![image-20221111151137081](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111151137081.png)

## 区块链浏览器

![image-20221111151557797](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111151557797.png)

### Hash值

![image-20221111151533729](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111151533729.png)

## 原料厂商管理

![image-20221111152524572](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111152524572.png)

## 物流管理

![image-20221111152551218](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111152551218.png)

## 生产厂商

### 产品加工

![image-20221111152640173](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111152640173.png)

### 工作记录

![image-20221111152746141](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111152746141.png)

#### 上报工作内容

![image-20221111152802737](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111152802737.png)

![image-20221111152811155](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111152811155.png)

### 零售商

#### 产品管理

![image-20221111153106742](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111153106742.png)

## 需要解决的问题

### 头像问题

![image-20221111152408578](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111152408578.png)

![image-20221111153220499](搭建基于Fabric的农产品溯源系统配置文档.assets/image-20221111153220499.png)

可能是头像没有图片的问题！！

### 标题问题

### 图片上传失败问题

### 上链的步骤到哪里
